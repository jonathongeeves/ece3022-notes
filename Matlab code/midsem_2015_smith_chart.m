clc
clear all
close all

%% Question 3 (c).
% Input parameters.
Z_L = 150 + 1i*25;
Z_0 = 50;
z_L = Z_L / Z_0;

% Creation of the smith chart for (c)
f = figure();
set(f, 'Position', [500 500 1000 1000]);
smithchart();
hold on

% Calculation of Gamma.
Gamma_L = (Z_L - Z_0) / (Z_L + Z_0);
g = abs(Gamma_L)
angle(Gamma_L) * 180/pi
plot(real(Gamma_L), imag(Gamma_L), '.', 'MarkerSize', 20);

% Calculation of VSWR.
VSWR = (1 + abs(Gamma_L)) / (1 - abs(Gamma_L))
plot(g, 0, '.k', 'MarkerSize', 20); 

% Calculation of load admittance.
y_L = 1 / z_L
gamma_y_L = (y_L - 1) / (y_L + 1);
plot(real(gamma_y_L), imag(gamma_y_L), 'm.', 'MarkerSize', 20);

a = 1 + 1i * 1.2;
gamma_a = (a - 1) / (a + 1);
plot(real(gamma_a), imag(gamma_a), 'g.', 'MarkerSize', 20);

x = 0 - 1i * 1.2;
gamma_x = (x - 1) / (x + 1);
plot(real(gamma_x), imag(gamma_x), 'r.', 'MarkerSize', 20);

theta = 0:0.01:2*pi;
plot(g * cos(theta), g * sin(theta), 'r'); 
legend('z_L', 'VSWR', 'y_L', 'a', 'x');

hgexport(f, '../Images/Exams/smith_chart_midsem.eps')

% Calculation of distance from y_L to a.
total_angle = pi + angle(gamma_y_L) + (pi - angle(gamma_a))
distance = total_angle / pi * 0.25

% Calculation of distance from 180 degrees to x.
total_angle = pi - angle(gamma_x)
distance = total_angle / pi * 0.25
