clc
clear all
close all

omega = 2 * pi;
beta = 2 * pi;

E = @(z, t) cos(omega *t - beta * z) + 0.5 * cos (omega * t + beta * z);

z = -2:0.01:0;

f = figure();
set(f, 'Position', [100, 100 1000, 400]);
hold on
for t = 0:0.01:1
   plot(z, E(z, t), 'k');
end
xlabel('z (in wavelenghts)');
ylabel('E');
hgexport(f, '../Images/Notes/standing_wave.eps');