clc
clear all
close all

%% Question 3.
c = 3e8;
f = @(m, n) c/2 * sqrt( (m/4.5)^2 + (n/4)^2 );

cutoff = 160e6;

for n = 0:10
    for m = 0:10
        if n + m > 0
            fc_mn = f(m, n);
            if fc_mn < cutoff
                fprintf('Mode with m = %d, n = %d, operates with cutoff %.3g MHz\n', m, n, fc_mn/1e6);
            end
        end
    end
end