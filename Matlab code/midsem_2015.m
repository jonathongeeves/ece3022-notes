clc
clear all
close all

%% Question 1.
% Provided parameters.
f = 2.45e9;
theta_i = 30;
signal_strength = 2.4e-6;

epsilon_r = 4;

c = 3e8;

% Part a.
beta_1 = 2 * pi * f / c
beta_2 = beta_1 * sqrt(epsilon_r)
theta_t = asind(1 /2 * sind(theta_i))

% Part b.
eta_1 = 120*pi;
eta_2 = 120*pi * 1/sqrt(epsilon_r);

gamma_TM = (eta_2 * cosd(theta_t) - eta_1 * cosd(theta_i)) / (eta_1 * cosd(theta_i) + eta_2 * cosd(theta_t))
tau_TM = (1 + gamma_TM) * cosd(theta_i) / cosd(theta_t)

% Part c.
E_0_t = tau_TM * signal_strength * 10^6

fprintf('\\vec{E}_s^t = e^{-j (%.3g x + %.3g z)} (%.3g \\vec{a}_x - %.3g \\vec{a}_z)\n', beta_2 * sind(theta_t), beta_2 * cosd(theta_t), E_0_t * cosd(theta_t), E_0_t * sind(theta_t)); 

%% Question 2.
% Provided parameters.
l = 5;
Z_in_reader = 150 + 1i*25;
epsilon_r = 2.45;
Z_0 = 50;

% Part a.
beta = 2 * pi * f / c * sqrt(epsilon_r)
beta_l = beta * l
tan(beta_l)

Z_L = Z_in_reader;

Z_in_transceiver = Z_0 * (Z_L + 1i * Z_0 * tan(beta_l)) / (Z_0 + 1i * Z_L * tan(beta_l))

% Part b.
Gamma_L = (Z_in_reader - Z_0) / (Z_in_reader + Z_0)
abs(Gamma_L)
angle(Gamma_L) * 180/pi

% Part c.
% c (i)
Gamma_L_t = (75 - Z_in_transceiver) / (75 + Z_in_transceiver)
abs(Gamma_L_t)
angle(Gamma_L_t) * 180/pi

% c (ii)
VSWR = (1 + abs(Gamma_L_t)) / (1 - abs(Gamma_L_t))

% c (iii)
P_t = (signal_strength * tau_TM)^2 / (2 * eta_2) * cosd(theta_t)

P_c_f = 1 - abs(Gamma_L)^2
P_t_f = 1 - abs(Gamma_L_t)^2