clc
clear all
close all

%% Example of finding a reflection coefficient from a Smith Chart.
Z_0 = 50;
Z_L = 50 + 1j * 100;

z_L = Z_L / Z_0;

f = figure();
smithchart();
hold on

% Plots gamma_L onto the Smith Chart.
gamma_L = (Z_L - Z_0 ) / (Z_L + Z_0);
plot(real(gamma_L), imag(gamma_L), '.', 'MarkerSize', 20);

% Plots a line from the origin to gamma_L.
plot([0 real(gamma_L)], [0 imag(gamma_L)], 'k--');

% Plots a circle.
r = abs(gamma_L);
theta = 0:0.01:2*pi;
plot(r * cos(theta), r * sin(theta), 'r');

hgexport(f, '../Images/Notes/smith_chart_reflection_coefficient.eps')

% Finds the input impedance.
length = 0.3; % Normalised to number of wavelengths.
angle_change = length / 0.5 * 360;

gamma_in = gamma_L * exp(1i * -angle_change * pi / 180);
plot(real(gamma_in), imag(gamma_in), 'm.', 'MarkerSize', 20);

% Plots a line from the origin to gamma_in.
plot([0 real(gamma_in)], [0 imag(gamma_in)], 'k--');

hgexport(f, '../Images/Notes/smith_chart_input_impedance.eps')