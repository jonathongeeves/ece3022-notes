clc
clear all
close all

%% Question 1.
l = 0.04;
f = 200e6;
I = 1e-3;
a = 1/2 * 25.35 * 25.4e-6

L_i = 2 * 10^-7 * l * (log(2 * l / a) - 1)
Z = 2 * pi * f*L_i 

V_A = 3 *I * Z
V_B = 2 * I * Z + V_A
V_C = I * Z + V_B

%% Question 2
l_1 = 0.04;
l_2 = 0.08;
l_3 = 0.12;

L = @(l) 2 * 10^-7 * l * (log(2 * l / a) - 1);

L_A = L(l_1)
L_B = L(l_2)
L_C = L(l_3)

V_A = L_A * 2 * pi * f * 1e-3
V_B = L_B * 2 * pi * f * 1e-3
V_C = L_C * 2 * pi * f * 1e-3

%% Quesiton 3.
SE = 20 * log10 (12e3/1e-9)

%% Question 4.
mu_0 = 1.256637e-6;;
mu_r = 5000;
sigma = 1e7;
f = 1e3;

alpha = sqrt(pi * f * mu_r * mu_0 * sigma)
alpha_db = alpha * 8.686
t = 20/alpha_db * 1e6

%% Question 6.
f = 1e9;
R = 10;
L = 10e-9;
C = 47e-12;

omega = 2*pi*f;

Z_1 = R + 1i*omega*L;
Z_C = 1 / (1i*omega*C);
Z_2 = 1 / (1 / Z_1 + 1 / Z_C);

V_Lf = Z_2 / (Z_1 + Z_2) * R / Z_1
abs(V_Lf)

IL = 20 * log10(1 / (2*abs(V_Lf)))

clear all

%% Question 7.
f = 40e6;
L = 12e-9;
C = 47e-12;
R = 10e3;

omega = 2 * pi * f;

Z_C = 1 / (1i*omega*C);
Z_L = 1i*omega*L;

Z_1 = 1 / (1/Z_C + 1/R);
Z_2 = Z_L + Z_1;
Z_3 = 1 / (1/Z_C + 1/Z_2);

V_Lf = Z_3/(R + Z_3) * Z_1/(Z_1 + Z_L)
abs(V_Lf)
IL = 20 * log10(1 / (2*abs(V_Lf)))


