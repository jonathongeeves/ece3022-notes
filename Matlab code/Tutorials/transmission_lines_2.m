clc
clear all
close all

%% Question 1.
fprintf('Question 1...\n');

% Provided parameters.
c = 3e8;
Z_0 = 74;
Z_s = 75;
Z_L = 100 + 1i * 125;
f = 600e6;
u_p = 0.8 * c;
l = 0.3;
V_s = 6;

% Part a.
fprintf('Part a\n');
lambda = u_p / f
Z_in = Z_0^2 / Z_L

% Part b.
fprintf('Part b\n');
V_in = V_s * Z_in / (Z_in + Z_s)
abs(V_in)
angle(V_in) * 180 / pi
beta_l = 2 * pi / lambda * l;

gamma_L = (Z_L - Z_0) / (Z_L + Z_0)
V_0_plus = V_in / (-1i + 1i * gamma_L)
abs(V_0_plus)
angle(V_0_plus) * 180 / pi

V_L = V_0_plus * (1 + gamma_L)
abs(V_L)
angle(V_L) * 180 / pi

%% Question 2.
fprintf('\nQuestion 2...\n');

% Provided parameters.
f = 100e6;
R = 5;
L = 0.01e-6;
G = 0.01;
C = 0.02e-9;
Z_L = 50 - 1i*25;
V_s = 10;
Z_s = 50;
l = 1;

omega = 2*pi*f;

% Find the voltage at the end of the T-line
gamma = sqrt((R + 1i * omega * L) * (G + 1i * omega * C))
Z_0 = sqrt((R + 1i*omega*L) / (G + 1i*omega*C))
Gamma_L = (Z_L - Z_0) / (Z_L + Z_0)
Z_in = Z_0 * (Z_L + Z_0 * tanh(gamma * l)) / (Z_0 + Z_L * tanh(gamma * l))
V_in = V_s * (Z_in) / (Z_in + Z_s)
V_L = V_in*(1 + Gamma_L) / (exp(gamma * l) + Gamma_L * exp(- gamma * l))
abs(V_L)
angle(V_L) * 180 / pi

%% Question 4.
fprintf('\nQuestion 4...\n');

% Provided parameters.
V_s = 10;
Z_s = 30;
Z_0 = 50;
c = 3e8;
u_p = 0.666*c;
Z_L = 150;
length = 0.1;

% Part a.
t_l = length / u_p
Gamma_L = (Z_L - Z_0) / (Z_L + Z_0)
Gamma_S = (Z_s - Z_0) / (Z_s + Z_0)
V_0 = V_s * Z_0  / (Z_0 + Z_s)

V_1 = V_0
V_2 = Gamma_L * V_0
V_3 = Gamma_S * Gamma_L * V_0
V_4 = Gamma_S * Gamma_L^2 * V_0

V_source = [V_1 V_1 (V_1 + V_2 + V_3) (V_1 + V_2 + V_3) (V_1 + V_2 + +V_3 + V_4) (V_1 + V_2 + V_3 + V_4)];
t_source = [0 2*t_l 2*t_l 4*t_l 4*t_l 4*t_l + 0.1e-9];

V_middle = [0 0 V_1 V_1 (V_1 + V_2) (V_1 + V_2) (V_1 + V_2 + V_3) (V_1 + V_2 + V_3) (V_1 + V_2 + V_3 + V_4) (V_1 + V_2 + V_3 + V_4)];
t_middle = [0 0.5*t_l 0.5*t_l 1.5*t_l 1.5*t_l 2.5*t_l 2.5*t_l 3.5*t_l 3.5*t_l 4*t_l + 0.1e-9];

V_end = [0 0 V_1+V_2 V_1+V_2 (V_1 + V_2 + V_3 + V_4) (V_1 + V_2 + V_3 + V_4)];
t_end = [0 t_l t_l 3*t_l 3*t_l 4*t_l + 0.1e-9];

f = figure();
hold on
plot(t_source, V_source);
plot(t_middle, V_middle, 'r');
plot(t_end, V_end, 'k');

legend('Source end', 'Middle', 'Load end', 'Location','southeast');
hgexport(f, '../../Images/T5_Q4_graph.eps')
