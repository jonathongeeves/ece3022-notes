clc
clear all
close all

%% Question 1
P_rad = 50e-3;
eta_0 = 120*pi;
r = 100;

P_d = P_rad / (4 * pi * r^2)
E = sqrt(2 * eta_0 * P_d)
E_rms = E/sqrt(2)*1000

E = E_rms * 10^(13 / 20)

%% Question 2
10^(1.85/10) * 100 - 100

%% Question 3
G = 33;
P_0 = 40;
r = 36000e3;

P_1 = P_0 * 10^(G/10)
P_d = P_1 / (4 * pi * r^2)

E = sqrt(2 * eta_0 * P_d)
20*log10(E)

%% Question 4
R_rad = 96*pi^2

%% Question 5.
f = figure();
theta = -pi/2:0.01:pi/2;
polar(theta, cos(theta))
hgexport(f, '../../Images/T8_Q5_1.eps')

f = figure();
theta = -pi/2:0.01:pi/2;
polar(theta, cos(theta).^2)
hgexport(f, '../../Images/T8_Q5_2.eps')

f = figure();
theta = -pi/2:0.01:pi/2;
polar(theta, cos(theta).^3)
hgexport(f, '../../Images/T8_Q5_3.eps')

%% Question 7
phi = -pi:0.001:pi;
E = abs(cos(pi/2 * cos(phi)));
f = figure();
polar(phi, E)
hgexport(f, '../../Images/T8_Q7.eps')

%% Question 8
phi = -pi:0.001:pi;
E = abs(cos(pi * cos(phi)));
f = figure();
polar(phi, E)
hgexport(f, '../../Images/T8_Q8.eps')

%% Question 8
phi = -pi:0.001:pi;
E = abs(cos(pi/2 * cos(phi) + pi/4));
f = figure();
polar(phi, E)
hgexport(f, '../../Images/T8_Q9.eps')