function [ u_list, v_list ] = get_propagation_constants( V )
u_list = [];
v_list = [];
epsilon = 1e-2;

theta = pi/2:-0.01:0;

u = V * cos(theta);
v = V * sin(theta);

even = abs(v - u .* tan(u));
odd = abs(v + u ./ tan(u));
for i = 2:length(even) - 1
   if even(i - 1) > even(i) && even(i) < even(i + 1)
      u_list(end + 1) = u(i);
      v_list(end + 1) = v(i);
   end
end

for i = 2:length(odd) - 1
   if odd(i - 1) > odd(i) && odd(i) < odd(i + 1)
      u_list(end + 1) = u(i);
      v_list(end + 1) = v(i);
   end
end

end

