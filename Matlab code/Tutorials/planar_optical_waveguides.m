clc
clear all
close all

%% Question  1.
c = 3e8;

f_1300 = c/(1300e-9) / 1e12
f_1550 = c/(1550e-9) / 1e12
f_1551 = c/(1551e-9) / 1e12

(f_1550 - f_1551) * 1e3

%% Question 2.
eta_1 = 1.505;
lambda = 1300e-9;
a = 5e-6;
k = 2*pi/lambda

% Part a.
V = pi/4
eta_2 = sqrt(eta_1^2 - (V / (k*a))^2)
eta_1 - eta_2

[u_list, v_list] = get_propagation_constants(V);
u_a = u_list / a;
v_a = v_list / a;
for i = 1:length(u_list)
    fprintf('u = %.4g, u/a = %.4g, v = %.4g, v/a = %.4g\n', u_list(i), u_a(i), v_list(i), v_a(i));
end

% Part b.
V = 5*pi/4
eta_2 = sqrt(eta_1^2 - (V / (k*a))^2)
eta_1 - eta_2 

[u_list, v_list] = get_propagation_constants(V);
u_a = u_list / a;
v_a = v_list / a;
for i = 1:length(u_list)
    fprintf('u = %.4g, u/a = %.4g, v = %.4g, v/a = %.4g\n', u_list(i), u_a(i), v_list(i), v_a(i));
end

%% Question 4.
% Part a.
lambda_ZD = 1290e-9;
lambda = 1552e-9;

D_M = 122 * (1 - lambda_ZD /lambda)

%Part c.
lambda = 1550e-9;
D_M = 122 * (1 - lambda_ZD /lambda)

lambda = 1550;
n_2 = 1.486;
delta = 0.003

V = sqrt((1.984 * n_2 * delta) / (10.465 * 1e-15 * c  * lambda))

V = 2.1;
delta = (10.465e-15 * c * lambda * V^2) / (1.984 * n_2)
lambda = lambda*1e-9;
a = V / (2 * pi / lambda * n_2 * sqrt(2 * delta))

lambda = (2 * pi * a * n_2 * sqrt(2*delta)) / 2.405 * 1e9
r_0 = a / log(2.405)