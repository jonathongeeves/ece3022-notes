clc
clear all
close all

%% Question 1.
fprintf('Question 1...\n');

Z_L = 15 + 1i*67;
Z_0 = 75;

f = figure();
set(f, 'Position', [500 500 1000 1000]);
smithchart();
hold on

z_L = Z_L / Z_0

gamma_L = (z_L - 1) / (z_L + 1);
plot(real(gamma_L), imag(gamma_L), 'r.', 'MarkerSize', 20);

plot(abs(gamma_L), 0, 'k.', 'MarkerSize', 20);

% Calculate the position in wavelengths towards generator for z_L.
lambda_l = (pi - angle(gamma_L)) / pi * 0.25 
lambda_in = lambda_l + 0.690 - 0.5

angle_in = pi - lambda_in / 0.25 * pi

plot(abs(gamma_L) * cos(angle_in), abs(gamma_L) * sin(angle_in), 'b.', 'MarkerSize', 20);

% Plot circle of constant gamma.
theta = 0:0.01:2*pi;
plot(abs(gamma_L) * cos(theta), abs(gamma_L) * sin(theta), 'r');

legend('z_L', 'VSWR', 'z_{in}');

hgexport(f, '../../Images/T5_Q2.eps')