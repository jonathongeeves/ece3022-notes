clc
clear all
close all

%% Question 1.
fprintf('Question 1...\n');

Z_in_sc = 1i*106;
Z_in_oc = -1i*23;
Z_in = 25 - 1i*70;
Z_0 = 50;

z_sc = Z_in_sc / Z_0;
z_oc = Z_in_oc / Z_0;
z_in = Z_in / Z_0;

f = figure();
set(f, 'Position', [500 500 1000 1000]);
smithchart();
hold on

gamma_sc = (z_sc - 1) / (z_sc + 1);
plot(real(gamma_sc), imag(gamma_sc), 'r.', 'MarkerSize', 20);

gamma_oc = (z_oc - 1) / (z_oc + 1);
plot(real(gamma_oc), imag(gamma_oc), 'b.', 'MarkerSize', 20);

gamma_in = (z_in - 1) / (z_in + 1);
plot(real(gamma_in), imag(gamma_in), 'm.', 'MarkerSize', 20);

% Calculate the location of z_l.
angle_in = pi + angle(gamma_in);
angle_sc = pi - angle(gamma_sc);

angle_L = angle_in + angle_sc - pi;
plot(abs(gamma_in) * cos(angle_L), abs(gamma_in) * sin(angle_L), 'k.', 'MarkerSize', 20);
plot([0 cos(angle_L)], [0 sin(angle_L)], 'k--');

% Plot circle of constant gamma.
theta = 0:0.01:2*pi;
plot(abs(gamma_in) * cos(theta), abs(gamma_in) * sin(theta), 'r');

legend('z_{in}^{sc}', 'z_{in}^{oc}', 'z_{in}', 'z_L');

hgexport(f, '../../Images/T5_Q1_2.eps')