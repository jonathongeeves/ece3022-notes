clc
clear all
close all

%% Question 1.
c = 3e8;
a = 0.02286;
b = 0.01143;

f = 16e9;
eta_0 = 120*pi;

% Calculation of ceofficients.
f_c = c/2 * 1/a
lambda_u = c/f
lambda = lambda_u / sqrt(1 - (f_c / f)^2)
u_G = c * sqrt(1 - (f_c / f)^2)
Z_10 = (120 * pi) / (sqrt(1 - (f_c / f)^2))

%% Question 2.
f = 10e9;
l = 0.2;

Z_10 = (120 * pi) / (sqrt(1 - (f_c / f)^2))

beta_u = 2 * pi * f / c
beta = beta_u * sqrt(1 - (f_c / f)^2)

tan(beta * l)

Z_in = 1i * Z_10 * tan(beta * l)
u_G = c * sqrt(1 - (f_c / f)^2)

% Question 4
a = 0.025;
f_c = c/2 * 1/a

lambda = (c / 9e9) / sqrt(1 - (6/9)^2)
u_p = 3 * 10^8 / sqrt(1 - (6/9)^2)

% Question 5
a = 0.025;
b = a / 2;

f_c_11 = c / 2 * sqrt(1 / a^2 + 1 / b^2)
f_c_11 = c / 2 * sqrt(4 / a^2 + 1 / b^2)
