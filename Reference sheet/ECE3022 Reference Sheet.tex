\documentclass[11pt,twocolumn]{article}
\usepackage[hmargin=1.8cm,vmargin=2cm]{geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{amsmath}
\usepackage{float}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{ltxtable}
\usepackage{tabu}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{url}
\usepackage[parfill]{parskip}
%\usepackage{listings}
\usepackage{float}
\usepackage{color}
\usepackage{textcomp}
\usepackage{caption}
\usepackage{pdfpages}
\usepackage[plain]{fancyref}
\usepackage{verbatim}
\usepackage{courier}
\usepackage[round]{natbib}
\usepackage[american voltages,american currents,straightlabels]{circuitikz}
\usepackage[bookmarks, hidelinks]{hyperref}
\usepackage{subcaption}
\usepackage{epstopdf}
\usepackage{booktabs}
\usepackage{enumitem}
\usepackage[compact]{titlesec}  
\titlespacing{\subsection}{0pt}{0pt}{0pt}
\titlespacing{\section}{0pt}{0pt}{0pt}

\setlength\abovedisplayskip{0pt}
  \setlength\belowdisplayskip{0pt}
  \setlength\abovedisplayshortskip{0pt}
  \setlength\belowdisplayshortskip{0pt}

\renewcommand{\vec}[1]{\boldsymbol{#1}}
%Page header and footer

%Ensures that the header does not overlap with the text
\setlength{\headheight}{15.2pt}
\setlength{\headsep}{0.7cm}

\setlist[itemize]{leftmargin=0.40cm}

% Add fancy styles
\pagestyle{fancy}
%\fancyhead{}
\fancyfoot{}

\lhead{}
\chead{}
\rhead{Reference Sheet}

% Footer
\lfoot{ECE3022}
\cfoot{\thepage}
\rfoot{Wireless and Guided Electromag}

\thispagestyle{fancy}

\begin{document}
\section{Plane waves}
This will deal with an oblique incidence case of a plane wave, most likely from air onto some media with $\mu_r = 1$ and $\epsilon_r \ne 1$. The plane wave will be polarised with either TE or TM. 

The angle of incidence will be either given in the question, or can be derived with basic geometry and trig. There is a chance that the angle of incidence is Brewster's Angle, which can be obtained using the formula sheet.

The parameters are given by
\begin{align*}
	\beta_1 &= \frac{\omega}{c}, \qquad \beta_2 = \frac{\omega}{c} \sqrt{\epsilon_r},	\\
	\eta_1 &= 120 \pi, \qquad \eta_2 = 120 \pi \sqrt{\frac{1}{\epsilon_r}}
\end{align*}

\subsection{Angle of transmission}
The angle of transmission should then be calculated using Snell's law:
\color{ForestGreen}
\begin{align*}
	\theta_t &= \sin^{-1} \left( \frac{\beta_1}{\beta_2} \sin \theta_i \right).
\end{align*}
\color{black}

\subsection{Transmission and reflection coefficients}
Use formulas for the dimensionless transmission and reflection coefficients. Take care about whether the wave is TE or TM polarised.
\color{ForestGreen}
\begin{align*}
	\Gamma_{TE} &= \frac{\eta_2 \cos \theta_i - \eta_1 \cos \theta_t}{\eta_2 \cos \theta_i + \eta_1 \cos \theta_t}	\\
	\tau_{TE} &= 1 + \Gamma_{TE}	\\
	\Gamma_{TM} &= \frac{\eta_2 \cos \theta_t - \eta_1 \cos \theta_i}{\eta_2 \cos \theta_i + \eta_1 \cos \theta_t} \\
	\tau_{TM} &= \frac{2 \eta_2 \cos \theta_i}{\eta_2 \cos \theta_i + \eta_1 \cos \theta_t}
\end{align*}
\color{black}

\subsection{Electric and magnetic fields}
Write down the electric and magnetic fields for incidence, reflection and transmission. This involves remembering (or deriving) the expressions for each of these fields, and evaluating them. Either $E_0^i$ or $H_0^i$ will be provided. Note that $E_0^i = \eta_1 H_0^i$. Also:
\begin{align*}
	E_0^r = \Gamma E_0^i, \qquad E_0^t = \tau E_0^t,
\end{align*}
for both TE and TM polarised fields. The \textbf{transmission coefficient should not be used to relate magnetic field strengths}, as the characteristic impedance of the media is different.

\subsubsection{TE polarization}
\begin{align*}
	\vec{E}^i &= E_0^i e^{-j \beta_1 (x \sin \theta_i + z \cos \theta_i)} \vec{a}_y	\\
	\vec{H}^i &= \frac{E_0^i}{\eta_1} e^{-j \beta_1 (x \sin \theta_i + z \cos \theta_i)} (-\cos \theta_i \vec{a}_x + \sin \theta_i \vec{a}_z)	\\
	\vec{E}^r &= E_0^r e^{-j \beta_1 (x \sin \theta_i - z \cos \theta_i)} \vec{a}_y	\\
	\vec{H}^r &= \frac{E_0^r}{\eta_1} e^{-j \beta_1 (x \sin \theta_i - z \cos \theta_i)} (\cos \theta_i \vec{a}_x + \sin \theta_i \vec{a}_z)	\\
	\vec{E}^t &= E_0^t e^{-j \beta_2 (x \sin \theta_t + z \cos \theta_t)} \vec{a}_y	\\
	\vec{H}^t &= \frac{E_0^t}{\eta_2} e^{-j \beta_2 (x \sin \theta_t + z \cos \theta_t)} (-\cos \theta_t \vec{a}_x + \sin \theta_t \vec{a}_z)
\end{align*}

\subsubsection{TM polarization}
\begin{align*}
	\vec{E}^i &= E_0^i e^{-j \beta_1 (x \sin \theta_i + z \cos \theta_i)} ( \cos \theta_i \vec{a}_x - \sin \theta_i \vec{a}_z)	\\
	\vec{H}^i &= \frac{E_0^i}{\eta_1} e^{-j \beta_1(x \sin \theta_i + z \cos \theta_i)} \vec{a}_y	\\
	\vec{E}^r &= E_0^r e^{-j \beta_1 (x \sin \theta_i - z \cos \theta_i)} ( \cos \theta_i \vec{a}_x + \sin \theta_i \vec{a}_z)	\\
	\vec{H}^r &= \frac{E_0^r}{\eta_1} e^{-j \beta_1(x \sin \theta_i - z \cos \theta_i)} \vec{a}_y	\\
	\vec{E}^t &= E_0^t e^{-j \beta_2 (x \sin \theta_t + z \cos \theta_t)} ( \cos \theta_t \vec{a}_x - \sin \theta_t \vec{a}_z)	\\
	\vec{H}^t &= \frac{E_0^t}{\eta_2} e^{-j \beta_2(x \sin \theta_t + z \cos \theta_t)} \vec{a}_y
\end{align*}

\subsection{Power transmitted}
The average power can be calculated as
\begin{align*}
	P_{ave} = \frac{1}{2} \text{Re} \left( \vec{E} \times \vec{H}^* \right),
\end{align*}
which is equivalent to
\begin{align*}
	P_{ave} &= \frac{|E_0^t|^2}{2 \eta_2},
\end{align*}
or the normal power, which is
\begin{align*}
	P_{ave,normal} &= \frac{|E_0^t|^2}{2 \eta_2} \cos \theta_t.
\end{align*}

\newpage
\section{T-Lines and Smith charts}

\subsection{Input and load impedance}
May have to show that the equation for input impedance of a lossless T-line:
\color{ForestGreen}
\begin{align*}
	Z_{in} = Z_0 \frac{Z_L + j Z_0 \tan(\beta \ell)}{Z_0 + j Z_L \tan(\beta \ell)}
\end{align*}
\color{black}
gives
\begin{align*}
	Z_L = Z_0 \frac{Z_{in} - j Z_0 \tan (\beta \ell)}{Z_0 - j Z_{in} \tan (\beta \ell)},
\end{align*}
which can be done using algebra.

Then this will have to be used to find either $Z_{in}$ or $Z_L$. Note that:
\begin{align*}
	\beta = \frac{2 \pi }{\lambda} = \frac{\omega}{v},
\end{align*}
and is measured in rad/m, hence $\tan(\beta \ell)$ should be computed using radians.

\subsection{Reflection coefficient}
The reflection coefficient at the load is
\begin{align*}
	\Gamma_L &= \frac{Z_L - Z_0}{Z_L + Z_0}.
\end{align*}

\subsection{Power delivered}
The power delivered to the load is
\begin{align*}
	P_t &= (1 - |\Gamma_L|^2) P_i.
\end{align*}

\subsection{Smith charts}

\subsubsection{Normalised load}
The normalised load is
\begin{align*}
	z_L = \frac{Z_L}{Z_0}.
\end{align*}
Plotting this on the smith chart provides $\Gamma_L$, which can also be verified as
\begin{align*}
	\Gamma_L = \frac{z_L - 1}{z_L + 1}.
\end{align*}
The intersection of the constant $|\Gamma|$ circle with the real axis provides VSWR, whose value can be verified using
\begin{align*}
	VSWR = \frac{1 + |\Gamma_L|}{1 - |\Gamma_L|}.
\end{align*}

\subsubsection{Quarter wave transformer}
A quarter wave transformer uses a section of T-line equal to $\lambda/4$ in length with characteristic impedance given by
\begin{align*}
	Z_T = \sqrt{Z_0 Z_L},
\end{align*}
where $Z_L$ is the impedance at the end of the quarter wave transformer. If the impedance at the load is complex, then a section of T-line should be placed between the quarter-wave transformer and the load such that the impedance becomes entirely real. The length of this section of T-line can be found by moving from $z_L$ to the real axis in \textbf{wavelengths towards load}.

\subsubsection{Stub matching}
Mark the load admittance, $y_L$, which is at the opposite side of the circle of constant $|\Gamma|$ as $z_L$. Then find the length of through through line and length of the stub:
\begin{enumerate}
	\item 
	The through line length is calculated as the clockwise distance from the point $y_L$ to the intersection of the constant $|\Gamma|$ circle with the line $1 \pm jx$, denote this point as $a$.
	
	\item
	Mark the point $b = \mp jx$, the length of an \text{open circuited} stub is the distance from $0\lambda$ to $b$, while the length of a \text{closed circuit} stub is the distance from $0.25 \lambda$ to $b$.
\end{enumerate}
Note that the choice of $a$ and $b$ will minimise either the length of the through line or the length of the stub (this may be specified by the question).

\newpage
\section{Waveguides}
A rectangular waveguide will be given with dimension $a \times b$, where $a$ is the larger dimension.

\subsection{Viable communication channels}
Calculate the cut-off frequency for the fundamental mode:
\begin{align*}
	f_{c_{10}} &= \frac{c}{2a},
\end{align*}
all frequencies below this can not propagate in the waveguide.

To calculate the cut-off frequency for higher order modes, use
\color{ForestGreen}
\begin{align*}
	f_{c_{nm}} = \frac{1}{2 \sqrt{\mu \epsilon}} \sqrt{ \left(\frac{m}{a}\right)^2 + \left(\frac{n}{b}\right)^2 } = \frac{\nu}{2}\sqrt{ \left(\frac{m}{a}\right)^2 + \left(\frac{n}{b}\right)^2 }
\end{align*}
\color{black}

\subsection{Modal dispersion}
Having a single mode (only the fundamental) ensures that there will be no modal dispersion caused by different modes in the waveguide travelling at different velocities.

\subsection{Allowing other frequencies to propagate}
In order to allow other channels (whose operating frequency are below the cut-off) to propagate in the tunel:
\begin{itemize}
	\item A leaky wire can be hung that radiates and transmits all frequency bands
	\item EBG side walls can be made for all pass propagation
\end{itemize}

\subsection{Field components}
Using the equations on the formula sheet, write down the field components. This requires:
\begin{align*}
	\beta = \beta_u \sqrt{1 - \left( \frac{f_c}{f} \right)^2}, \qquad \beta = \frac{2 \pi}{\lambda}
\end{align*}

\subsection{Power propagation}
A formula is provided on the sheet that gives the average power propagation in the tunnel in $\text{W}$. This can be related to breakdown voltage, which typically $3 \times 10^6 \text{ W/m}$ for air. Using a different dielectric will typically increase the breakdown voltage.

\subsection{Attenuation}
The attenuation of an evanescent mode (non-propagating mode) is
\color{ForestGreen}
\begin{align*}
	\alpha = k_c \sqrt{1 - \left( \frac{f}{f_c} \right)^2},
\end{align*}
\color{black}
where $k_c = \sqrt{\left( \frac{m \pi}{a} \right)^2 + \left( \frac{n \pi }{b} \right)^2}$. Note that $\alpha$ is measure in Np/m, where 
\begin{align*}
	1 \text{ Np} = \frac{20}{\ln 10} = 8.686 \text{ dB}.
\end{align*}

\subsection{Impedances}
Waveguide impedance may be required:
\begin{align*}
	Z_{mn}^{TE} = \frac{\eta_u}{\sqrt{1 - \left( \frac{f_c}{f}  \right)^2}}	\qquad
	Z_{mn}^{TM} = \eta_u \sqrt{ 1 - \left( \frac{f_c}{f} \right)^2  }
\end{align*}

\newpage
\section{Optical fibres}

\subsection{Attenuation}
	\begin{figure}[H]
		\centering
		\includegraphics[width=8cm]{../Images/T9_Q3}
	\end{figure}
	
	\textbf{Memorization time:} The drop in attenuation from 800 nm to 1550 nm is due to Rayleigh scattering, which is proportional to $1/\lambda^4$. The increase in attenuation from 1550 nm to 1800 nm is caused by Infra-red absorption. The peaks around 925 nm and 1400 nm arise due water contamination. 
	
	The regions of concern for us are windows around 810 nm, 1310 nm and 1550 nm. The window around 810 nm was used in the early 1970s due to the availability of laser sources, detectors and multimode optical fibers. In 1980s, new types of lasers and detectors were developed for 1310 nm window and 1550 nm window. Hence, these are the windows that are commonly used in current day optical communications. The window around 1550 nm has the lowest attenuation (0.2 dB/km), while the next lowest attenuation is for 1310 nm window (0.3 dB/km). Since the 1550 nm window
	has the lowest attenuation, it is going to become the main region of operation used for long-haul transmission.

\subsection{Launch angle and numerical aperture}
The launch angle $\theta_a$ is the angle from the normal at which light enters. Using trig we can relate the maximum value of $\theta_a$ to  $\theta_c$, which ensures total internal reflection, then use Snell's law to derive an expression for $\theta_i$ in terms of the refractive indices. The numerical aperture (NA) is
\color{ForestGreen}
\begin{align*}
	NA = \sin \theta_a = \frac{\sqrt{n_1^2 - n_2^2}}{n_0}
\end{align*}
\color{black}

\subsection{Material dispersion}
The material dispersion in optical fibres is approximated as
\color{ForestGreen}
\begin{align*}
	D_M(\lambda) = 122 \left( 1 - \frac{\lambda_{ZD}}{\lambda} \right)
\end{align*}
\color{black}
where $\lambda_{ZD} = 1290 \text{ nm}$. Note that the unit of dispersion is  $\text{ps/nm}\cdot\text{km}$. It is possible that we get given a $n(\lambda)$ and evaluate
\color{ForestGreen}
\begin{align*}
	D_M(\lambda) = - \frac{\lambda}{c} \frac{d^2 n}{d \lambda^2},
\end{align*}
\color{black}
then solve for $D_M(\lambda) = 0$ in order to find $\lambda_{ZD}$.

\subsection{Waveguide dispersion}
Having found the material dispersion, we want to determine the value of $V$ in order to create compensating waveguide dispersion to give a required value of $D_{T}$ using
\begin{align*}
	D_{T} = D_M + D_W,
\end{align*}
noting that $D_M$ is normally positive and $D_W$ is normally negative. So use
\color{ForestGreen}
\begin{align*}
	D_W(V) = -\frac{n_2 \Delta}{c \lambda} \left[ V \frac{\partial^2 (V b(V))}{\partial V^2} \right], b(V) = \left( p - \frac{q}{V} \right)^2
\end{align*}
\color{black}
for known (from formula sheet) values of $p, q$ to show that
\begin{align*}
	D_W(V) = -\frac{2 n_2 \Delta q^2}{c \lambda V^2}, V = \sqrt{ - \frac{2 n^2 \Delta q^2}{10^{-15} c \lambda D_W} },
\end{align*}
where $\lambda$ is measured in nm. Note that the power of $10^{-15}$ is required because to convert between units.

\subsubsection{Does $V$ make sense physically}
We will likely be asked whether $V$ makes sense physically. The answer is not always yes, but the same justifications can be used either way. The approximation used for $V$ requires that $1.7 < V < 2.2$, if $V$ is outside this range then the answer does not make sense. In addition, we require that $2 < V < 2.2$ for most power to be contained in the core, so a $V$ in this range is ideal.

\subsection{Radius of the fibre}
The normalised frequency and radius of the core can be related using
\begin{align*}
	V = k a n_2 \sqrt{2 \Delta},\qquad  k = \frac{2 \pi }{\lambda}
\end{align*}
Expect that $a$ is in the order of micrometers. Note that this equation can be used to find the \textbf{cut-off wavelength} by substuting in in $V = 2.405$.

\subsection{Mode spot size}
Mode spot size is given by
\color{ForestGreen}
\begin{align*}
	r = \frac{a}{\ln V}
\end{align*}
\color{black}

\section{Antennas}

\subsection{Definitions}
There are a number of definitions that may be asked for antennas. They are too wordy to state here, but know:
\begin{itemize}
	\item Non-radiating near-field, radiating near-field and far-field
	\item Gain (how is it measured in dBi and dBd)
	\item Patten solid angle (measured in steradians)
	\item Beamwidth
	\item Bandwidth
	\item Front-to-back ratio
\end{itemize}

\subsection{Gain}
Gain is calculated as
\begin{align*}
	G = 10 \log_{10} \left( \frac{P_1}{P_0} \right),
\end{align*}
where $P_1$ is the antenna under test and $P_0$ is a reference antenna. If $P_0$ is an isotropic antenna then the measurement is in dBi, and if $P_0$ is a dipole then the measurement is in dBd. They are related as
\begin{align*}
	\left( G \right)_{\text{dBi}} = \left( G \right)_{\text{dBd}} + 2.15.
\end{align*}

\subsection{Power}
Power can be measured in W, mw, dB, or dBm. Converting between these are common questions.

\subsubsection{Power density}
The power density at a point is given by
\begin{align*}
	P_d = \frac{|\vec{E}|^2}{2 \eta_0},
\end{align*}
where $|\vec{E}|$ is the known electric field. Note that the Poynting vector magnitude is the same as the power density.

\subsubsection{Radiated power}
Radiated power is related to power density
\begin{align*}
	P_{rad} &= 4 \pi r^2 P_d.
\end{align*}

\subsubsection{$R_{rad}$}
The value of $R_{rad}$ can be found by solving
\color{ForestGreen}
\begin{align*}
	P_{rad} = \int \vec{P}(r, \theta, \phi) d \vec{S} = \frac{1}{2} I_0^2 R_{rad},
\end{align*}
\color{black}
where $\vec{S} = r^2 \sin \theta d \theta d \phi \vec{a}_r$.

\subsection{Pattern solid angle and directivity}
Pattern solid angle is calculated as
\color{ForestGreen}
\begin{align*}
	\Omega_p = \int_0^{2 \pi} \int_0^{\pi} P_n(\theta, \phi) \sin \theta d \theta d \phi,
\end{align*}
\color{black}
then directivity is
\color{ForestGreen}
\begin{align*}
	D = \frac{4 \pi}{\Omega_p}.
\end{align*}
\color{black}

\subsection{Array of radiators}
The field from two radiators at points $x_1$ and $x_2$ in the X-Z plane is
\begin{align*}
	E = \frac{A_0}{r} e^{-j \beta r} e^{-j \beta x_2 \sin \theta} + \frac{A_0}{r} e^{-j \beta r} e^{j \beta x_1 \sin \theta}
\end{align*}
in the X-Y plane it is
\begin{align*}
	E = \frac{A_0}{r} e^{-j \beta r} e^{-j \beta x_2 \cos \phi}  + \frac{A_0}{r} e^{-j \beta r} e^{j \beta x_1 \cos \phi}
\end{align*}
where $A_0 = \sqrt{60 G P_r}$ V/m. This then can normally be rearranged and simplified using Euler's formula. The radiation pattern (where everything but the angular magnitude is normalised to 1) may then be plotted using a calculator and brute force. See the final question of antennas tutorial for several examples of questions like this.

\newpage
\section{EMI/EMC}
\subsection{Definitions}
\begin{itemize}
	\item EMI (sources of EMI) and EMC
	\item Non-ideal component behaviour
	\item Shields, grounds and filters
\end{itemize}

\subsection{Equivalent circuits}

\subsubsection{Resistor}
\begin{figure}[ht]
	\centering
	\begin{circuitikz}
		\draw (0,0) to [L=$L_L$,o-] (2,0);
		\draw (2,0) to [L=$L_X$] (4,0);
		\draw (4,0) to [R=$R$] (6,0);
		\draw (6,0) to [short,-o] (7,0);
		\draw (2,0) to [short] (2,-1);
		\draw (2,-1) to [C = $C_X$] (6,-1);
		\draw (6,-1) to [short] (6,0); 
	\end{circuitikz}
	\caption{Equivalent resistor model}
	\label{fig:equivalent_resistor}
\end{figure}
$L_X$ is the parasitic element inductance (negligible except when dealing with wire resistors). $C_X$ is the parasitic element capacitance. $L_L$ is the parasitic lead inductance, which is from the lead at both ends.

\subsubsection{Inductor}
\begin{figure}[ht]
	\centering
	\begin{circuitikz}
		\draw (0,0) to [short,o-] (4,0);
		\draw (0,4) to [short,o-] (4,4);
		\draw (2,0) to [C=$C_X$] (2,4);
		\draw (4,0) to [R=$R_X$] (4,2);
		\draw (4,2) to [L=$L$] (4,4);
	\end{circuitikz}
\end{figure}
$R_X$ is the parasitic resistance which arises from ohmic losses in the wire. This accounts for hysteresis and eddy-current losses in the magnetic core. $C_X$ is the parasitic capacitance that arises from electric fields established between windings.

\subsubsection{Capacitor equivalent circuit model}
\begin{figure}[ht]
	\centering
	\begin{circuitikz}
		\draw (2,0) to [short] (4,0);
		\draw (2,2) to [short] (4,2);
		\draw (2,0) to [C=$C$] (2,2);
		\draw (4,0) to [R=$R_\delta$] (4,2);
		\draw (0,0) to [L=$L_L$,o-] (2,0);
		\draw (0,2) to [R=$R_X$,o-] (2,2);
	\end{circuitikz}
\end{figure}
$R_X$ is the series resistance that arises due to ohmic losses (negligible for ceramic capacitors).  $L_L$ is the parasitic lead inductance. $R_\delta$ is the loss tangent resistance (typically very large, so can be ignored).

\subsubsection{Self resonance frequency}
It can be shown that self resonance frequency for inductors and capacitors occurs at
\begin{align*}
	\omega^2 L C = 1.
\end{align*}
For ideal behaviour, inductors and capacitors should be operated at less than half the self resonance frequency.

\subsection{Conductors}
The \textbf{external inductance of a conductor} is given by
\color{ForestGreen}
\begin{align*}
	L = 0.2 \ell \left[ \ln \left( \frac{2 \ell}{a} \right)  - 1 \right] \text{ nH}.
\end{align*}
\color{black}
The resistance of a conductor is
\begin{align*}
	R_{DC} = \frac{\ell}{\sigma \pi a^2}, \qquad R_{AC} = \frac{\ell}{\sigma 2 \pi a \delta},
\end{align*}
where $\delta$ is the skin depth
\color{ForestGreen}
\begin{align*}
	\delta = \frac{1}{\sqrt{\pi f \mu \sigma}}.
\end{align*}
\color{black}
Then the total impedance is $Z = R + j \omega L$. Generally the reactive component will dominate.

\subsection{Insertion loss}
Insertion loss is a measure of the effectiveness of a filter (high is good). It is defined as
\begin{align*}
	IL = 20 \log_{10} \left( \frac{|V_L|}{V_{Lf}|} \right) \text{ dB},
\end{align*}
where $V_L$ is the load voltage, and $V_{Lf}$ is the filtered load voltage. Note that shunt capacitors have high IL for high source and load resistances, while series inductors work well for small resistances.

\subsection{Attenuation (shields)}
Attenuation in Np/m is given by
\color{ForestGreen}
\begin{align*}
	\alpha =\sqrt{\pi f \mu \sigma} = \frac{1}{\delta}.
\end{align*}
\color{black}
Convert to dB and multiply by shield thickness to get total attenuation.

\end{document}

