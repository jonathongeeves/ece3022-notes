\documentclass[a4paper]{article}

%Jonathon's Stuff
\usepackage[hmargin=2cm,vmargin=3cm,bindingoffset=0.5cm]{geometry}
\usepackage{amsmath}
\usepackage{float}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{ltxtable}
\usepackage{tabu}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{url}
\usepackage[parfill]{parskip}
\usepackage{listings}
\usepackage{color}
\usepackage{textcomp}
\usepackage{caption}
\usepackage[lofdepth,lotdepth]{subfig}
\usepackage{pdfpages}
\usepackage[plain]{fancyref}
\usepackage{verbatim}
\usepackage{courier}
\usepackage[square, numbers]{natbib}
\usepackage{booktabs}
\usepackage[bookmarks, hidelinks]{hyperref}
\usepackage{enumitem}
\usepackage{epstopdf}
\usepackage{pdflscape}

\bibliographystyle{unsrtnat}

\DeclareGraphicsExtensions{.pdf,.png,.jpg,.eps}

\numberwithin{equation}{section}
\numberwithin{figure}{section}
\numberwithin{table}{section}

\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\arraystretch}{1.2} 
\setcounter{tocdepth}{2}

\setitemize{itemsep=0pt}

\definecolor{listinggray}{gray}{0.6}
\definecolor{lbcolor}{rgb}{1,1,1}
\lstset{
	backgroundcolor=\color{white},
	tabsize=4,
	rulecolor=,
	language=matlab,
	basicstyle=\ttfamily\scriptsize,
	upquote=true,
	aboveskip={1.5\baselineskip},
	columns=fixed,
	showstringspaces=true,
	extendedchars=true,
	breaklines=true,
	prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
	frame=single,
	showtabs=false,
	showspaces=false,
	showstringspaces=false,
	identifierstyle=\ttfamily,
	keywordstyle=\color[rgb]{0,0,1},
	commentstyle=\color[rgb]{0.133,0.545,0.133},
	stringstyle=\color[rgb]{0.627,0.126,0.941},
}

%Page header and footer

%Ensures that the header does not overlap with the text
\setlength{\headheight}{15.2pt}
\setlength{\headsep}{0.7cm}

% Add fancy styles
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}

% Header
\lhead{ECE3022}
\rhead{Design project}

% Footer
\lfoot{Group 69}
\cfoot{\thepage}
\rfoot{Jonathon Geeves and David Stojanovski}
%End header and footer

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % New command to make the lines in the title page
\begin{document}
	\begin{titlepage}
		\begin{center}
			
			\HRule \\[0.4cm] % Horizontal line
			\textsc{\LARGE \\ECE3022 - Wireless and guided electromagnetism}\\[0.5cm]
			\textsc{\LARGE {\bf Patch antenna array}}\\[0.5cm]
			\HRule \\[1.5cm] % Horizontal line
			\textit{\Large Jonathon Geeves David Stojanovski}\\[0.5cm]
			
			\begin{minipage}{0.4\textwidth}
				\LARGE
				
			\end{minipage}
			\begin{minipage}{0.4\textwidth}
			\end{minipage}\\[2cm]
			
		\end{center}
		
	\end{titlepage}
	
	\author{Jonathon Geeves}
	
	
	
	%End title details
	
	%Prevents the footer being placed on this page
	
	\thispagestyle{empty}
	
	\newpage
	
	\setcounter{page}{1}
	\pagenumbering{Roman}
	
	%Table of contents
	\tableofcontents
	%\listoffigures
	%\listoftables
	
	%Begin report
	
	\newpage
	
	\cfoot{Page \thepage\ of \pageref{LastPage}}
	
	\setcounter{page}{1}
	\pagenumbering{arabic}

\section{Introduction}
The goal of this design project is to create a micro-strip patch antenna array that operates at the resonant frequency of 2.630 GHz. The antenna was designed using theory and CST Microwave Studio, which would be used to simulate and optimise various design parameters. Once the antenna design was fabricated, it was then tested in a laboratory environment.

\section{Design}
A micro-strip patch antenna consists of a rectangular patch whose dimensions are determined by the target resonant frequency, and a transmission line to allow input and output of signals. A micro-strip patch antenna array consists of several patch antennas operating together that are connected together with an impedance matched interconnect network. This project would create a patch antenna array that used two individual antennas. A general sketch of the design is shown in \fref{fig:antenna}.

\begin{figure}[H]
	\centering
	\includegraphics[width=6cm]{Images/antenna}
	\caption{The integrated patch antenna array}
	\label{fig:antenna}
\end{figure}

\subsection{Patch dimensions}
To design the width and length of the patch antenna we must first know:
\begin{itemize}
	\item Substrate relative permittivity $\epsilon_r = 4.3$
	\item Resonant frequency $f_r = 2.63$ GHz
	\item Substrate height $h = 1.6$ mm
\end{itemize}
Then we can find the patch width, $W$,
\begin{align*}
	W = \frac{1}{2 f_r \sqrt{\epsilon_0 \mu_0}} \sqrt{\frac{2}{\epsilon_r +1}}.
\end{align*}
Using our known values for $f_r$ and $\epsilon_r$, we can can calculate the patch width,
\begin{align*}
W &= \frac{1}{2 \times 2.630 \times 10^9 \sqrt{\epsilon_0 \mu_0}} \sqrt{\frac{2}{4.3 +1}},\\
&= 35.012 \textrm{ mm}
\end{align*}
This can be used to find the effective relative permittivity using our known substrate height,
\begin{align*}
	\epsilon_{reff} &= \frac{\epsilon_r + 1}{2} + \frac{\epsilon_r - 1}{2} \left(1 + 12 \frac{h}{W}\right)^{-1/2},\\
	&= \frac{4.3 + 1}{2} + \frac{4.3 - 1}{2} \left(1 + 12 \times \frac{1.6 \times 10^{-3}}{35.012 \times 10^{-3}}\right)^{-1/2},\\
	&= 3.976
\end{align*}
This allows for the calculation of the patch length. First, we calculate $\Delta L$,
\begin{align*}
	\Delta L &= 0.412 h \frac{ \left( \epsilon_{reff} + 0.3 \right) \left( \frac{W}{h} + 0.264 \right) }{ \left( \epsilon_{reff} - 0.257 \right) \left( \frac{W}{h} + 0.8 \right)},	\\
	&=  0.412 \times 1.6 \times 10^{-3} \frac{ \left( 3.976 + 0.3 \right) \left( \frac{35.012 \times 10^{-3}}{1.6 \times 10^{-3}} + 0.264 \right) }{ \left( 3.976 - 0.257 \right) \left( \frac{35.012 \times 10^{-3}}{1.6 \times 10^{-3}} + 0.8 \right)},\\
	&= 0.7402\textrm{ mm}
\end{align*}
We can then use this to calculate the patch length, $L$,
\begin{align*}
	L &= \frac{1}{2 f_r \sqrt{\epsilon_{reff}} \sqrt{\epsilon_0 \mu_0}} - 2 \Delta L,\\
	&= \frac{1}{2 \times 2.63 \times 10^9 \sqrt{3.976} \sqrt{\epsilon_0 \mu_0}} - 2\times 0.7402 \times 10^{-3},\\
	&= 27.103 \textrm{ mm}
\end{align*}
So the width and length of the patch, respectively, is $W = 35.012$ mm and $L = 27.103$ mm.

\subsection{Micro-strip feed line}
We want to feed our antenna using a micro-strip feed line with an impedance of $50 \Omega$. We are using a known copper height of 0.017 mm, so we can work out the appropriate width of the copper line in order to achieve an impedance of $50 \Omega$. The Advanced Design Studios (ADS) LineCalc tool enables us to do this, and provides a value of 3.09 mm.

\subsubsection{Connection to patch}
The micro-strip feed line is then connected at the midpoint of the wider edge of the antenna patch. At the point of connection, there should be two insets into the patch, as seen in \fref{fig:antenna}. The length and width of these insets is not determined analytically, and is instead found through simulation, which will be discussed further later.

\subsubsection{Feed line corner}
The feed line must make a $90^\circ$ turn in order to reach the transmission line network that connects both patches together with the input. At this corner, a slice of the corner at $45^\circ$ should be cut off in order to produce optimal behaviour. The depth of this slice was determined via simulation.

\subsection{Patch duplications}
Once the first patch was designed, as second identical patch must be placed onto the antenna. The centre of this second patch should be $0.7 \lambda$ away from the centre of the first patch, where $\lambda$ is the wavelength at our design frequency. We calculated this, and found that the patch centres must be 79.848 mm apart.

\subsection{Power divider}
As there are two patch antennas, each with an impedance of $50 \Omega$, the total load impedance of the antenna array will be $100 \Omega$. However, we want to feed it with a $50 \Omega$ input line. If we do not perform impedance matching, then this mismatch will create signal reflection and cause our antenna to behave poorly. 

The matching technique used was a quarter-wavelength impedance transformer. This uses a quarter-wavelength portion of transmission line (13.743 mm for out design frequency) with characteristic impedance calculated as
\begin{align*}
	Z_{\lambda/4} = \sqrt{Z_1 Z_2},
\end{align*}
where $Z_1$ is characteristic impedance of the input line ($50 \Omega$) and $Z_2$ is the total impedance of the load ($100 \Omega$). This evaluates to $Z_{\lambda/4} = 70.7 \Omega.$ We can again use ADS LineCalc to determine the appropriate width of the copper micro-strip, which is 1.63 mm.

\subsubsection{Middle notch}
The power divider should have a small triangular notch inset in the centre of the quarter-wavelength transformer. The depth and width of this notch is not calculated analytically, and is instead found through simulation.

\subsection{Simulation}
A number of parameters for the antenna design are not calculated analytically, and are instead determined via simulation using CST Microwave Studio. Each time a simulation is performed we look at the $S_{11}$ value at 2.630 GHz, and aim to get the lowest value we can. This is done by first using a parameter sweep to determine an approximate value for each parameter.

Once approximate values for each parameter are found, we can use the CST optimiser tool. This allows us to specify a minimum and maximum range for a number of parameters, along with an optimisation goal. An allowance of $\pm 20 \%$ for each parameter was chosen, and the optimisation goal was to minimize $S_{11}$ at 2.630 GHz. The optimisation algorithm used was called Trust Region Framework.

\subsubsection{Simulation results}
The optimiser was able to determine parameters that gave us very good simulation results. This is shown in \fref{fig:s11}.

\begin{figure}[H]
	\centering
	\includegraphics[width=16cm]{Images/s11}
	\caption{$S_{11}$ simulation of full antenna array}
	\label{fig:s11}
\end{figure}

The simulation results showed that at our target resonant frequency the $S_{11}$ parameter was $-36.161$ dB, well below our target value of less than $-10$ dB.

The 3D polar radiation pattern obtained from the far-field monitor is shown in \fref{fig:farfield}. The 2D polar radiation pattern is shown in \fref{fig:polar}.

\begin{figure}[H]
	\centering
	\includegraphics[width=14cm]{Images/farfield}
	\caption{3D polar simulation of full antenna array}
	\label{fig:farfield}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=14cm]{Images/polar}
	\caption{2D polar simulation of full antenna array}
	\label{fig:polar}
\end{figure}

\section{Results}
Our micro-strip patch antenna array was fabricated. We soldered a coaxial port at the input to the antenna and tested it using equipment in the lab. An antenna transmitted at a range of frequencies from 1.7 GHz to 3.5 GHz. The received power of the designed antenna was measured across this frequency range, and the maximum received power was noted. The results are shown in \fref{fig:transmitter}.

\begin{figure}[H]
	\centering
	\includegraphics[width=12cm]{Images/transmitter}
	\caption{Power received by designed antenna}
	\label{fig:transmitter}
\end{figure}

The reflected power of the designed antenna ($S_{11}$) was measured at a range of frequencies from 1.3 GHz to 3.3 GHz, and the minimum was noted. This is the resonant frequency of the antenna.

\begin{figure}[H]
	\centering
	\includegraphics[width=12cm]{Images/resonance}
	\caption{$S_{11}$ of designed antenna}
	\label{fig:resonance}
\end{figure}

The results of the testing, along with the original design parameters are shown in \fref{tab:results}.

\begin{table}[H]
	\centering
	\begin{tabular}{l l l}
		\toprule[0.8pt]
			& \textbf{Design requirement} & \textbf{Measured value} \\
		\midrule[0.8pt]
		Resonant frequency & 2.630 GHz & 2.690 GHz	\\
		Received maximum & 2.630 GHz & 2.670 GHz	\\
		$S_{11}$ minimum & $< -10$ dB & $-17.945$ dB 	\\
		\bottomrule[0.8pt]
	\end{tabular}
	\caption{Comparison of design requirements and measured results}
	\label{tab:results}
\end{table}
The resonant frequency and the frequency for which the antenna received the maximum transmission were off by 60 MHz and 40 MHz respectively. The required accuracy threshold was within 120 MHz of the design frequency, which we were able to achieve. The measured value of $S_{11}$ was $-17.945$ dB, which was below the required threshold of $-10$ dB.

\section{Discussion}
The designed antenna worked well, and had a resonant frequency quite close to the designed resonant frequency, however, it was 60 MHz higher than anticipated. One potential reason for this is the substrate material, which was FR4. During the design and simulation of our antenna we assumed that the relative permittivity of the FR4 substrate was 4.3. However FR4 is a glass-reinforced epoxy laminate, and the exact value of the permittivity will vary with the construction of the FR4. As such, it is reasonable to expect that the relative permittivity of our manufactured antenna was different to 4.3. This is consistent with the results of others in the class, who also found that the resonant frequency of their manufactured antenna was higher than the expected frequency based on their simulations.

Another difference between our simulation results and the actual testing results was the minimum $S_{11}$ peak. In out simulation we obtained a minimum peak of $-36.8$ dB, while in the lab test we achieved $-17.9$ dB. A likely cause of this change is the manufacturing tolerances in the dimensions of the copper tracks.

\section{Conclusion}

By using analytical calculations of the dimensions of the patch antenna, micro-strip feed lines and power divider, we obtained some approximate values that would give an antenna that performs reasonably closely to our design frequencies. Other parameters in the design, such as the dimensions of the corner of the feed line or the notch in the power divider, could not be analytically calculated and had to be found by optimisation. All of the dimensions in the design, including those that had an approximate analytical value, were parameterised. By using CST's optimisation algorithms on these parameters, we could find optimal values of all of the dimensions to bring our resonant frequency as close as possible to the required value of 2.630 GHz. Further optimisation was used to ensure that $S_{11}$ value of the antenna less than $-10$ dB. The fabricated antenna had resonance around 2.690 GHz with $S_{11}$ less than $-10$ dB meaning the resonant frequency was approximately 60 MHz higher than the design value. However, this was still within the allowable range for resonant frequency. The discrepancy is likely the result of the FR4 having a different relative permittivity as well as dimensions of the etched copper being slightly different within manufacturing tolerances.

\end{document}